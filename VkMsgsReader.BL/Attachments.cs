﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace VkMsgsReader.BL
{
    public class VkAttachmentFactory
    {
        private readonly Dictionary<string, Type> _registeredAttachmentTypes = new Dictionary<string, Type>();

        public VkAttachmentFactory Register(string type, Type parser)
        {
            var interfaceType = typeof(IVkAttachment);
            if (parser.GetInterface(interfaceType.Name) == interfaceType)
                _registeredAttachmentTypes.Add(type, parser);
            else
                throw new ArgumentException("Type must be an implementation of the IVkAttachment class!");

            return this;
        }

        public VkAttachmentFactory Register(Dictionary<string, Type> types)
        {
            foreach (var type in types)
                Register(type.Key, type.Value);

            return this;
        }

        public IVkAttachment GetAttachment(JToken attachment)
        {
            var attachmentType = (string) attachment["type"];
            IVkAttachment createdObject = null;

            if (_registeredAttachmentTypes.Keys.Contains(attachmentType))
            {
                var ctor = _registeredAttachmentTypes[attachmentType].GetConstructor(new Type[] { attachment.GetType() });
                createdObject = (IVkAttachment) ctor?.Invoke(new object[] { attachment });
            }

            return createdObject ?? new VkUnregisteredAttachment(attachmentType);
        }
    }

    public interface IVkAttachment
    {
        string Name { get; }
        string Description { get; }
        string Link { get; }
        string ImageLink { get; }
        bool Clickable { get; }
    }

    public abstract class VkLocalizedAttachmentBase : IVkAttachment
    {
        protected abstract Dictionary<string, Dictionary<LocalizedMessage, string>> LocalizedStrings { get; }

        protected enum LocalizedMessage
        {
            Name, UnknownName,
            DescriptionMissing,
            LinkMissing
        }

        protected string GetLocalizedMessage(LocalizedMessage requestedMessage, params object[] substitutions)
        {
            var language = CultureInfo.CurrentUICulture.IetfLanguageTag;

            if (LocalizedStrings.ContainsKey(language))
                return string.Format(LocalizedStrings[language][requestedMessage], substitutions);
            if (LocalizedStrings.ContainsKey("en-EN"))
                return string.Format(LocalizedStrings["en-EN"][requestedMessage], substitutions);

            return "";
        }

        protected string GetOrDefault(object value, LocalizedMessage defaultValue)
        {
            return (value == null || value.ToString() == "")
                ? GetLocalizedMessage(defaultValue)
                : value.ToString();
        }

        protected string GetOrDefault(object value, string defaultValue)
        {
            return (value == null || value.ToString() == "")
                ? defaultValue
                : value.ToString();
        }

        protected string SubstituteOrDefault(LocalizedMessage defaultValue, LocalizedMessage message, params object[] values)
        {
            foreach (var value in values)
                if (value == null || value.ToString() == "")
                    return GetLocalizedMessage(defaultValue);

            return GetLocalizedMessage(message, values);
        }

        public abstract string Name { get; }
        public abstract string Description { get; }
        public abstract string Link { get; }
        public abstract string ImageLink { get; }
        public abstract bool Clickable { get; }
    }

    public class VkUnregisteredAttachment : VkLocalizedAttachmentBase
    {
        protected override Dictionary<string, Dictionary<LocalizedMessage, string>> LocalizedStrings { get; }

        internal VkUnregisteredAttachment(string type)
        {
            LocalizedStrings = new Dictionary<string, Dictionary<LocalizedMessage, string>>()
            {
                { "ru-RU", new Dictionary<LocalizedMessage, string>()
                {
                    { LocalizedMessage.Name, $"<{type}>" },
                    { LocalizedMessage.DescriptionMissing, "Пока данный тип вложений не поддерживается приложением." }
                } },

                { "en-EN", new Dictionary<LocalizedMessage, string>()
                {
                    { LocalizedMessage.Name, $"<{type}>" },
                    { LocalizedMessage.DescriptionMissing, "Currently, this type of attachments isn't supported by the application." }
                } }
            };
        }

        public override string Name => GetLocalizedMessage(LocalizedMessage.Name);
        public override string Description => GetLocalizedMessage(LocalizedMessage.DescriptionMissing);
        public override string Link => null;
        public override string ImageLink => null;
        public override bool Clickable => false;
    }


    public sealed class VkPhotoAttachment : VkLocalizedAttachmentBase {
        public override string Name { get; }
        public override string Description { get; }
        public override string Link { get; }
        public override string ImageLink { get; }
        public override bool Clickable { get; }

        protected override Dictionary<string, Dictionary<LocalizedMessage, string>> LocalizedStrings { get; } = new Dictionary<string, Dictionary<LocalizedMessage, string>>()
        {
            { "ru-RU", new Dictionary<LocalizedMessage, string>()
            {
                { LocalizedMessage.Name, "Изображение №{0}_{1}" },
                { LocalizedMessage.UnknownName, "<Безымянное изображение>" },
                { LocalizedMessage.DescriptionMissing, "<Описание отсутствует>" },
                { LocalizedMessage.LinkMissing, "<Ссылка отсутствует>" }
            } },

            { "en-EN", new Dictionary<LocalizedMessage, string>()
            {
                { LocalizedMessage.Name, "Image #{0}_{1}" },
                { LocalizedMessage.UnknownName, "<Unknown photo>" },
                { LocalizedMessage.DescriptionMissing, "<The description is missing>" },
                { LocalizedMessage.LinkMissing, "<There is no link>" }
            }}
        };

        public VkPhotoAttachment(JToken attachment)
        {
            var photo = attachment["photo"];

            Name = SubstituteOrDefault(LocalizedMessage.UnknownName, LocalizedMessage.Name, photo["owner_id"], photo["id"]);

            Description = GetOrDefault(photo["text"], LocalizedMessage.DescriptionMissing);

            Link = GetOrDefault(photo["photo_2560"] ??
                                photo["photo_1280"] ??
                                photo["photo_807"] ??
                                photo["photo_604"] ??
                                photo["photo_130"] ??
                                photo["photo_75"],
                                LocalizedMessage.LinkMissing);

            ImageLink = (string) (photo["photo_604"] ??
                                  photo["photo_130"] ??
                                  photo["photo_75"]) ??
                                  "";

            Clickable = (Link != GetLocalizedMessage(LocalizedMessage.LinkMissing));
        }
    }

    public sealed class VkVideoAttachment : VkLocalizedAttachmentBase
    {
        protected override Dictionary<string, Dictionary<LocalizedMessage, string>> LocalizedStrings { get; } = new Dictionary<string, Dictionary<LocalizedMessage, string>>()
        {
            { "ru-RU", new Dictionary<LocalizedMessage, string>()
            {
                { LocalizedMessage.Name, "Видео «{0}»" },
                { LocalizedMessage.UnknownName, "<Неизвестная видеозапись>" },
                { LocalizedMessage.DescriptionMissing, "<Описание отсутствует>" },
                { LocalizedMessage.LinkMissing, "<Не удалось получить ссылку на видеозапись>" }
            } },

            { "en-EN", new Dictionary<LocalizedMessage, string>()
            {
                { LocalizedMessage.Name, "Video \"{0}\"" },
                { LocalizedMessage.UnknownName, "<Unknown video>" },
                { LocalizedMessage.DescriptionMissing, "<The description is missing>" },
                { LocalizedMessage.LinkMissing, "<Couldn't fetch a link to the video>" }
            } }
        };
        public override string Name { get; }
        public override string Description { get; }
        public override string Link { get; }
        public override string ImageLink { get; }
        public override bool Clickable { get; } = false;

        public VkVideoAttachment(JToken attachment)
        {
            var video = attachment["video"];

            Name = SubstituteOrDefault(LocalizedMessage.UnknownName, LocalizedMessage.Name, video["title"]);

            Description = GetOrDefault(video["description"], LocalizedMessage.DescriptionMissing);

            var owner = (string) (video["owner_id"] ?? "");
            var id = (string) (video["id"] ?? "");
            if (!string.IsNullOrWhiteSpace(owner) && !string.IsNullOrWhiteSpace(id))
            {
                Link = $"https://vk.com/video{owner}_{id}";
                Clickable = true;
            }
            else
            {
                Link = GetLocalizedMessage(LocalizedMessage.LinkMissing);
                Clickable = false;
            }

            ImageLink = (string) (video["photo_800"] ??
                                  video["photo_640"] ??
                                  video["photo_320"] ??
                                  video["photo_130"]) ??
                                  "";
        }
    }

    public sealed class VkStickerAttachment : VkLocalizedAttachmentBase
    {
        protected override Dictionary<string, Dictionary<LocalizedMessage, string>> LocalizedStrings { get; } = new Dictionary<string, Dictionary<LocalizedMessage, string>>()
        {
            { "ru-RU", new Dictionary<LocalizedMessage, string>()
            {
                { LocalizedMessage.Name, "Стикер №{0}_{1}" },
                { LocalizedMessage.UnknownName, "<Неизвестный стикер>" },
                { LocalizedMessage.DescriptionMissing, "<У стикеров нет описания>" },
                { LocalizedMessage.LinkMissing, "<Не удалось получить ссылку на изображение>" }
            } },
            { "en-EN", new Dictionary<LocalizedMessage, string>()
            {
                { LocalizedMessage.Name, "Sticker #{0}_{1}" },
                { LocalizedMessage.UnknownName, "<Unknown sticker>" },
                { LocalizedMessage.DescriptionMissing, "<Stickers don't have a description>" },
                { LocalizedMessage.LinkMissing, "<Couldn't fetch a link to the sticker image>" }
            } }
        };

        public override string Name { get; }
        public override string Description { get; }
        public override string Link { get; }
        public override string ImageLink { get; }
        public override bool Clickable { get; }

        public VkStickerAttachment(JToken attachment)
        {
            var sticker = attachment["sticker"];

            Name = SubstituteOrDefault(LocalizedMessage.UnknownName, LocalizedMessage.Name, sticker["product_id"], sticker["id"]);

            Description = GetLocalizedMessage(LocalizedMessage.DescriptionMissing);

            ImageLink = (string) (sticker["photo_352"] ??
                                  sticker["photo_256"] ??
                                  sticker["photo_128"] ??
                                  sticker["photo_64"]) ??
                                  "";

            Link = GetOrDefault(ImageLink, LocalizedMessage.LinkMissing);

            Clickable = (ImageLink != "");
        }
    }

    public sealed class VkGiftAttachment : VkLocalizedAttachmentBase
    {
        protected override Dictionary<string, Dictionary<LocalizedMessage, string>> LocalizedStrings { get; } = new Dictionary<string, Dictionary<LocalizedMessage, string>>()
        {
            { "ru-RU", new Dictionary<LocalizedMessage, string>()
            {
                { LocalizedMessage.Name, "Подарок №{0}" },
                { LocalizedMessage.UnknownName, "<Неизвестный подарок>" },
                { LocalizedMessage.DescriptionMissing, "<Описание у подарков отсутствует>" },
                { LocalizedMessage.LinkMissing, "<Не удалось получить ссылку на изображение>" }
            } },

            { "en-EN", new Dictionary<LocalizedMessage, string>()
            {
                { LocalizedMessage.Name, "Gift #{0}" },
                { LocalizedMessage.UnknownName, "<Unknown gift>" },
                { LocalizedMessage.DescriptionMissing, "<Gifts don't have a description>" },
                { LocalizedMessage.LinkMissing, "<Couldn't fetch a link to the image>" }
            } }
        };
        public override string Name { get; }
        public override string Description { get; }
        public override string Link { get; }
        public override string ImageLink { get; }
        public override bool Clickable { get; }

        public VkGiftAttachment(JToken attachment)
        {
            var gift = attachment["gift"];

            Name = SubstituteOrDefault(LocalizedMessage.UnknownName, LocalizedMessage.Name, gift["id"]);

            Description = GetLocalizedMessage(LocalizedMessage.DescriptionMissing);
            
            ImageLink = (string) (gift["thumb_256"] ??
                                  gift["thumb_96"] ??
                                  gift["thumb_48"]) ??
                                  "";

            Link = GetOrDefault(ImageLink, LocalizedMessage.LinkMissing);

            Clickable = (ImageLink != "");
        }
    }

    public sealed class VkLinkAttachment : VkLocalizedAttachmentBase
    {
        protected override Dictionary<string, Dictionary<LocalizedMessage, string>> LocalizedStrings { get; } = new Dictionary<string, Dictionary<LocalizedMessage, string>>()
        {
            { "ru-RU", new Dictionary<LocalizedMessage, string>()
            {
                { LocalizedMessage.Name, "Ссылка: «{0}»" },
                { LocalizedMessage.UnknownName, "<Неизвестная ссылка>" },
                { LocalizedMessage.DescriptionMissing, "<Описание отсутствует>" },
                { LocalizedMessage.LinkMissing, "<Не удалось получить ссылку>" }
            } },

            { "en-EN", new Dictionary<LocalizedMessage, string>()
            {
                { LocalizedMessage.Name, "Link: \"{0}\"" },
                { LocalizedMessage.UnknownName, "<Unknown link>" },
                { LocalizedMessage.DescriptionMissing, "<The description is missing>" },
                { LocalizedMessage.LinkMissing, "<Couldn't fetch the link>" }
            } }
        };
        public override string Name { get; }
        public override string Description { get; }
        public override string Link { get; }
        public override string ImageLink { get; }
        public override bool Clickable { get; }

        public VkLinkAttachment(JToken attachment)
        {
            var link = attachment["link"];

            Name = SubstituteOrDefault(LocalizedMessage.UnknownName, LocalizedMessage.Name, link["title"]);

            Description = GetOrDefault(link["description"], LocalizedMessage.DescriptionMissing);

            Link = GetOrDefault(link["url"], LocalizedMessage.LinkMissing);

            ImageLink = GetOrDefault(link["photo"], "");

            Clickable = (Link != GetLocalizedMessage(LocalizedMessage.LinkMissing));
        }
    }

    public sealed class VkAudioAttachment : VkLocalizedAttachmentBase
    {
        protected override Dictionary<string, Dictionary<LocalizedMessage, string>> LocalizedStrings { get; } = new Dictionary<string, Dictionary<LocalizedMessage, string>>()
        {
            { "ru-RU", new Dictionary<LocalizedMessage, string>()
            {
                { LocalizedMessage.Name, "Аудио: {0} — {1} ({2})" },
                { LocalizedMessage.UnknownName, "<Неизвестная аудиозапись>" },
                { LocalizedMessage.DescriptionMissing, "<На данный момент отображение текста для аудиозаписей не поддерживается>" },
                { LocalizedMessage.LinkMissing, "<Не удалось получить ссылку на аудиофайл>" }
            } },

            { "en-EN", new Dictionary<LocalizedMessage, string>()
            {
                { LocalizedMessage.Name, "Audio: {0} — {1} ({2})" },
                { LocalizedMessage.UnknownName, "<Unknown audio>" },
                { LocalizedMessage.DescriptionMissing, "<Currently, the application cannot show you the lyrics of the song>" },
                { LocalizedMessage.LinkMissing, "<Couldn't fetch a link to the audio file>" }
            } }
        };

        public override string Name { get; }
        public override string Description { get; }
        public override string Link { get; }
        public override string ImageLink => "";
        public override bool Clickable => false;

        public VkAudioAttachment(JToken attachment)
        {
            var audio = attachment["audio"];

            var durationInSeconds = (uint) (audio["duration"] ?? 0);
            var duration = TimeSpan.FromSeconds(durationInSeconds).ToString(@"mm\:ss");
            Name = SubstituteOrDefault(LocalizedMessage.UnknownName, LocalizedMessage.Name, audio["artist"], audio["title"], duration);

            Description = GetLocalizedMessage(LocalizedMessage.DescriptionMissing);

            Link = GetOrDefault(audio["url"], LocalizedMessage.LinkMissing);
        }
    }

    public sealed class VkDocAttachment : VkLocalizedAttachmentBase
    {
        protected override Dictionary<string, Dictionary<LocalizedMessage, string>> LocalizedStrings { get; } = new Dictionary<string, Dictionary<LocalizedMessage, string>>()
        {
            { "ru-RU", new Dictionary<LocalizedMessage, string>()
            {
                { LocalizedMessage.Name, "Документ: {0} ({1}, {2:0.00} Кб)" },
                { LocalizedMessage.UnknownName, "<Неизвестный документ>" },
                { LocalizedMessage.DescriptionMissing, "<У документов нет описания>" },
                { LocalizedMessage.LinkMissing, "<Не удалось получить ссылку на документ>" }
            } },

            { "en-EN", new Dictionary<LocalizedMessage, string>()
            {
                { LocalizedMessage.Name, "Document: {0} ({1}, {2:0.00} KB)" },
                { LocalizedMessage.UnknownName, "<Unknown document>" },
                { LocalizedMessage.DescriptionMissing, "<Documents don't have a description>" },
                { LocalizedMessage.LinkMissing, "<Couldn't fetch a link to the document>" }
            } }
        };

        public override string Name { get; }
        public override string Description { get; }
        public override string Link { get; }
        public override string ImageLink { get; }
        public override bool Clickable { get; }

        public VkDocAttachment(JToken attachment)
        {
            var doc = attachment["doc"];

            var size = (double) (doc["size"] ?? 0) / 1024;
            Name = SubstituteOrDefault(LocalizedMessage.UnknownName, LocalizedMessage.Name, doc["title"], doc["ext"], size);

            Description = GetLocalizedMessage(LocalizedMessage.DescriptionMissing);

            Link = GetOrDefault(doc["url"], LocalizedMessage.LinkMissing);

            ImageLink = (string) (doc["photo_130"] ??
                                  doc["photo_100"]) ??
                                  "";

            Clickable = (ImageLink != null);
        }
    }

    public sealed class VkWallAttachment : VkLocalizedAttachmentBase
    {
        protected override Dictionary<string, Dictionary<LocalizedMessage, string>> LocalizedStrings { get; } = new Dictionary<string, Dictionary<LocalizedMessage, string>>()
        {
            { "ru-RU", new Dictionary<LocalizedMessage, string>()
            {
                { LocalizedMessage.Name, "Запись на стене №{0}_{1}" },
                { LocalizedMessage.UnknownName, "<Неизвестная запись на стене>" },
                { LocalizedMessage.DescriptionMissing, "<Текст в записи отсутствует>" },
                { LocalizedMessage.LinkMissing, "<Не удалось получить ссылку на запись>" }
            } },

            { "en-EN", new Dictionary<LocalizedMessage, string>()
            {
                { LocalizedMessage.Name, "Post on the wall №{0}_{1}" },
                { LocalizedMessage.UnknownName, "<Unknown post on the wall>" },
                { LocalizedMessage.DescriptionMissing, "<Text of the post is missing>" },
                { LocalizedMessage.LinkMissing, "<Couldn't fetch a link to the post>" }
            } }
        };

        public override string Name { get; }
        public override string Description { get; }
        public override string Link { get; }
        public override string ImageLink => "";
        public override bool Clickable => false;

        public VkWallAttachment(JToken attachment)
        {
            var wall = attachment["wall"];

            Name = SubstituteOrDefault(LocalizedMessage.UnknownName, LocalizedMessage.Name, wall["from_id"], wall["id"]);

            Description = GetOrDefault(wall["text"], LocalizedMessage.DescriptionMissing);

            Link = Name != GetLocalizedMessage(LocalizedMessage.UnknownName)
                ? $"https://vk.com/wall{wall["from_id"]}_{wall["id"]}"
                : GetLocalizedMessage(LocalizedMessage.LinkMissing);
        }
    }
}
