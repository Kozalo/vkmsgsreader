﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace VkMsgsReader.BL
{
    public class VkUser
    {
        public int ID { get; }
        public string Name { get; internal set; }

        public VkUser(int id, string name)
        {
            ID = id;
            Name = name;
        }

        public VkUser(int id)
            : this(id, id.ToString())
        {}
    }

    public class VkUserResolver
    {
        private readonly VkApi _api;
        private readonly List<VkUser> _usersToRename = new List<VkUser>();

        public VkUserResolver(VkApi api)
        {
            _api = api;
        }

        public VkUserResolver(VkApi api, IEnumerable<VkUser> usersToRename)
            :this(api)
        {
            _usersToRename.AddRange(usersToRename);
        }

        public void AddUser(VkUser userToRename)
        {
            _usersToRename.Add(userToRename);
        }

        public async Task ResolveNames()
        {
            var uids = from user in _usersToRename select user.ID;
            var resolvedDict = await _api.GetUserNames(uids.Distinct());

            foreach (var user in _usersToRename)
            {
                if (resolvedDict.ContainsKey(user.ID))
                    user.Name = resolvedDict[user.ID];
            }
        }
    }

    public class VkApi
    {
        private const int ChatIdOffset = 2000000000;
        private const float CurrentApiVersion = 5.60F;
        private readonly string _apiUrl = @"https://api.vk.com/method/{{0}}?access_token={0}&v={1}";
        private int _currentUserId;

        public const uint VkMaxCount = 100;

        public VkApi(string accessToken)
        {
            _apiUrl = string.Format(_apiUrl, accessToken, CurrentApiVersion);
            FillCurrentUserId();
        }

        private string GetApiUrl(string methodName, Dictionary<string, object> parameters = null)
        {
            var methodUrl = string.Format(_apiUrl, methodName);

            if (parameters != null)
                foreach (var param in parameters)
                    methodUrl += $"&{param.Key}={param.Value}";

            return methodUrl;
        }

        private static async Task<JToken> ApiRequest(string apiUrl)
        {
            var client = new WebClient {Encoding = Encoding.UTF8};
            var response = await client.DownloadStringTaskAsync(apiUrl);
            var parsedResponse = JObject.Parse(response);
            return parsedResponse["response"];
        }

        public async Task<Dictionary<int, string>> GetUserNames(IEnumerable<int> uids)
        {
            var uidsString = string.Join(",", uids);

            var parsedResponse = await ApiRequest(GetApiUrl("users.get", new Dictionary<string, object>()
            {
                { "user_ids", uidsString }
            }));

            var users = new Dictionary<int, string>();
            foreach (var user in parsedResponse)
            {
                var name = user["first_name"] + " " + user["last_name"];
                users.Add((int) user["id"], name);
            }

            return users;
        }

        private async void FillCurrentUserId()
        {
            var parsedResponse = await ApiRequest(GetApiUrl("users.get"));
            _currentUserId = (int) parsedResponse[0]["id"];
        }

        public async Task<VkDialogsInfo> GetDialogs(uint offset, uint count)
        {
            var parsedResponse = await ApiRequest(GetApiUrl("messages.getDialogs", new Dictionary<string, object>()
            {
                {"offset", offset},
                {"count", count}
            }));

            var conversations = new List<VkDialog>();
            var userResolver = new VkUserResolver(this);
            foreach (var item in parsedResponse["items"])
            {
                VkDialog conv;
                if (item["chat_id"] != null)
                    conv = new VkDialog((int) item["chat_id"], (string) item["title"]);
                else
                {
                    var user = new VkUser((int) item["user_id"]);
                    conv = new VkDialog(user);
                    userResolver.AddUser(user);
                }

                conversations.Add(conv);
            }

            await userResolver.ResolveNames();

            return new VkDialogsInfo((uint) parsedResponse["count"], conversations.ToArray());
        }

        public async Task<VkMessagesInfo> GetMessages(VkDialog dialog, uint offset)
        {
            var peerId = (!dialog.IsChat) ? dialog.Id : dialog.Id + ChatIdOffset;

            var parsedResponse = await ApiRequest(GetApiUrl("messages.search", new Dictionary<string, object>()
            {
                {"peer_id", peerId},
                {"date", DateTime.Now.ToString("ddMMyyyy")},
                {"offset", offset},
                {"count", VkMaxCount}
            }));

            var userResolver = new VkUserResolver(this);
            var messageList = new List<VkMessage>();

            ParseMessages(parsedResponse["items"], messageList, userResolver);

            await userResolver.ResolveNames();
            return new VkMessagesInfo((uint) parsedResponse["count"], messageList.ToArray());
        }

        private void ParseMessages(JToken messages, ICollection<VkMessage> listToFill, VkUserResolver userResolver, uint recursionDepth = 0)
        {
            var attachmentResolver = GetAttachmentParser();
            var attachmentList = new List<IVkAttachment>();

            foreach (var message in messages)
            {
                var localRecursionDepth = recursionDepth;

                int uid;
                if (localRecursionDepth == 0)
                {
                    var outValue = (int) message["out"];
                    uid = (outValue == 1) ? _currentUserId : (int) message["user_id"];
                }
                else
                    uid = (int) message["user_id"];

                var user = new VkUser(uid);
                userResolver.AddUser(user);

                attachmentList.Clear();
                if (message["attachments"] != null)
                    attachmentList.AddRange(
                        message["attachments"].Select(attachment => attachmentResolver.GetAttachment(attachment)));

                var msg = (localRecursionDepth == 0)
                    ? new VkMessage((int) message["id"], (int) message["date"], (int) message["out"], user, (string) message["body"], attachmentList.ToArray())
                    : new VkForwardedMessage((int)message["date"], user, (string)message["body"], attachmentList.ToArray(), localRecursionDepth);

                listToFill.Add(msg);

                if (message["fwd_messages"] != null)
                    ParseMessages(message["fwd_messages"], listToFill, userResolver, ++localRecursionDepth);
            }
        }

        internal static VkAttachmentFactory GetAttachmentParser()
        {
            return new VkAttachmentFactory()
                .Register("photo", typeof(VkPhotoAttachment))
                .Register("video", typeof(VkVideoAttachment))
                .Register("audio", typeof(VkAudioAttachment))
                .Register("sticker", typeof(VkStickerAttachment))
                .Register("gift", typeof(VkGiftAttachment))
                .Register("link", typeof(VkLinkAttachment))
                .Register("doc", typeof(VkDocAttachment))
                .Register("wall", typeof(VkWallAttachment));
        }
    }
}
