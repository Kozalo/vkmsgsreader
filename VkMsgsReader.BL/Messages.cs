﻿using System;

namespace VkMsgsReader.BL
{
    public class VkMessagesInfo
    {
        public uint Count { get; }
        public VkMessage[] Messages { get; }

        public VkMessagesInfo(uint count, VkMessage[] messages)
        {
            Count = count;
            Messages = messages;
        }
    }

    public interface IVkMessage
    {
        DateTime Date { get; }
        bool Outgoing { get; }
        VkUser User { get; }
        string Text { get; }
        IVkAttachment[] Attachments { get; }
    }

    public class VkMessage : IVkMessage
    {
        private const string NoTextPlaceholder = "<сообщение без текста>";

        public int Id { get; }
        public DateTime Date { get; }
        public bool Outgoing { get; }
        public VkUser User { get; }
        public string Text { get; }
        public IVkAttachment[] Attachments { get; }

        public VkMessage(int id, int timestamp, int isOut, VkUser user, string text, IVkAttachment[] attachments)
        {
            Id = id;
            Date = DateTimeOffset.FromUnixTimeSeconds(timestamp).UtcDateTime.ToLocalTime();
            Outgoing = (isOut == 1);
            User = user;
            Text = text != "" ? text : NoTextPlaceholder;
            Attachments = attachments;
        }
    }

    public class VkForwardedMessage : VkMessage
    {
        public VkForwardedMessage(int timestamp, VkUser user, string text, IVkAttachment[] attachments, uint nestingDepth)
            :base(0, timestamp, 0, user, new string('>', (int) nestingDepth) + ' ' + text, attachments)
        {}
    }
}
