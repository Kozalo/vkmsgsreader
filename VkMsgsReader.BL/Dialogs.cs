﻿namespace VkMsgsReader.BL
{
    public class VkDialog
    {
        private readonly VkUser _user;
        private readonly int _id;
        private readonly string _name;

        public int Id => IsChat ? _id : _user.ID;
        public string Name => IsChat ? _name : _user.Name;

        public bool IsChat { get; }

        public VkDialog(int id, string name)
        {
            _id = id;
            _name = name;
            IsChat = true;
        }

        public VkDialog(VkUser user)
        {
            _user = user;
            IsChat = false;
        }
    }

    public class VkDialogsInfo
    {
        public uint Count { get; private set; }
        public VkDialog[] Dialogs { get; private set; }

        public VkDialogsInfo(uint count, VkDialog[] dialogs)
        {
            Count = count;
            Dialogs = dialogs;
        }
    }
}
