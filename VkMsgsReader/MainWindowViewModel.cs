﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using VkMsgsReader.BL;

namespace VkMsgsReader
{
    public class MainWindowViewModel : ViewModelBase
    {
        // Must be redefined as a positive number!
        private const uint VkAppId = 0;

        #region Properties
            private VkApi _api;
            private string _token = "";
            private BindingList<VkDialog> _dialogs = new BindingList<VkDialog>();
            private BindingList<VkMessageView> _messages = new BindingList<VkMessageView>();
            private uint _dialogPage;
            private uint _messagePage;
            private uint _dialogPageCount;
            private uint _messagePageCount;
            private bool _isApiWorking;
            private object _selectedDialog;
            private object _selectedMessage;

            public string Token
            {
                get { return _token; }
                set { Set(ref _token, value, broadcast: true); }
            }

            public BindingList<VkDialog> Dialogs
            {
                get { return _dialogs; }
                set { Set(ref _dialogs, value); }
            }

            public uint DialogPage
            {
                get { return _dialogPage; }
                private set { Set(ref _dialogPage, value, broadcast: true); }
            }

            public uint MesssagePage
            {
                get { return _messagePage; }
                set { Set(ref _messagePage, value, broadcast: true); }
            }

            public uint DialogPageCount
            {
                get { return _dialogPageCount; }
                private set { Set(ref _dialogPageCount, value, broadcast: true); }
            }

            public uint MessagePageCount
            {
                get { return _messagePageCount; }
                private set { Set(ref _messagePageCount, value, broadcast: true); }
            }

            private VkApi Api
            {
                get { return _api; }
                set { Set(ref _api, value, broadcast: true); }
            }

            public bool IsApiWorking
            {
                get { return _isApiWorking; }
                private set { Set(ref _isApiWorking, value, broadcast: true); }
            }

            public bool IsApiActive => Api != null && !IsApiWorking;

            public BindingList<VkMessageView> Messages
            {
                get { return _messages; }
                set { Set(ref _messages, value, broadcast: true); }
            }

            public object SelectedDialog
            {
                get { return _selectedDialog; }
                set { Set(ref _selectedDialog, value, broadcast: true); }
            }

            public object SelectedMessage
            {
                get { return _selectedMessage; }
                set
                {
                    var view = value as VkMessageView;
                    if (view != null)
                    {
                        Attachments.Clear();
                        foreach (var attachment in view.Attachments)
                            Attachments.Add(attachment);
                    }

                    Set(ref _selectedMessage, value);
                }
            }

            public BindingList<IVkAttachment> Attachments { get; } = new BindingList<IVkAttachment>();

        #endregion

        #region Commands
            private RelayCommand _getTokenCommand;
            private AutoRelayCommand _getDialogsCommand;
            private AutoRelayCommand _nextDialogPageCommand;
            private AutoRelayCommand _previousDialogPageCommand;
            private AutoRelayCommand _getMessagesCommand;
            private AutoRelayCommand _previousMessagePageCommand;
            private AutoRelayCommand _nextMessagePageCommand;
            private AutoRelayCommand _setMessagePageCommand;
            private RelayCommand<string> _openAttachmentCommand;

            public RelayCommand GetTokenCommand
            {
                get
                {
                    return _getTokenCommand ?? (_getTokenCommand =
                               new RelayCommand(() => Process.Start(
                                    $"https://oauth.vk.com/authorize?client_id={VkAppId}&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=messages,offline&response_type=token&v=5.68"
                               ))
                           );
                }
            }

            public AutoRelayCommand GetDialogsCommand
            {
                get
                {
                    if (_getDialogsCommand == null)
                    {
                        _getDialogsCommand = new AutoRelayCommand(InitApi, () => !string.IsNullOrWhiteSpace(Token));
                        _getDialogsCommand.DependsOn(() => Token);
                    }
                
                    return _getDialogsCommand;
                }
            }

            public AutoRelayCommand NextDialogPageCommand
            {
                get
                {
                    if (_nextDialogPageCommand == null)
                    {
                        _nextDialogPageCommand = new AutoRelayCommand(() => CallApi(GetDialogs, ++DialogPage), () => IsApiActive && DialogPage < DialogPageCount);
                        _nextDialogPageCommand.DependsOn(() => Api);
                        _nextDialogPageCommand.DependsOn(() => IsApiWorking);
                        _nextDialogPageCommand.DependsOn(() => DialogPage);
                        _nextDialogPageCommand.DependsOn(() => DialogPageCount);
                    }

                    return _nextDialogPageCommand;
                }
            }

            public AutoRelayCommand PreviousDialogPageCommand
            {
                get
                {
                    if (_previousDialogPageCommand == null)
                    {
                        _previousDialogPageCommand = new AutoRelayCommand(() => CallApi(GetDialogs, --DialogPage), () => IsApiActive && DialogPage > 1);
                        _previousDialogPageCommand.DependsOn(() => Api);
                        _previousDialogPageCommand.DependsOn(() => IsApiWorking);
                        _previousDialogPageCommand.DependsOn(() => DialogPage);
                    }

                    return _previousDialogPageCommand;
                }
            }

            public AutoRelayCommand GetMessagesCommand
            {
                get
                {
                    if (_getMessagesCommand == null)
                    {
                        _getMessagesCommand = new AutoRelayCommand(() => CallApi(GetMessages, new GetMessagesArguments((VkDialog) SelectedDialog, 1)), () => IsApiActive && SelectedDialog != null);
                        _getMessagesCommand.DependsOn(() => Api);
                        _getMessagesCommand.DependsOn(() => IsApiWorking);
                        _getMessagesCommand.DependsOn(() => SelectedDialog);
                    }

                    return _getMessagesCommand;
                }
            }

            public AutoRelayCommand PreviousMessagePageCommand
            {
                get
                {
                    if (_previousMessagePageCommand == null)
                    {
                        _previousMessagePageCommand = new AutoRelayCommand(() => CallApi(GetMessages, new GetMessagesArguments((VkDialog) SelectedDialog, --MesssagePage)), () => IsApiActive && SelectedDialog != null && MesssagePage > 1);
                        _previousMessagePageCommand.DependsOn(() => Api);
                        _previousMessagePageCommand.DependsOn(() => IsApiWorking);
                        _previousMessagePageCommand.DependsOn(() => SelectedDialog);
                        _previousMessagePageCommand.DependsOn(() => MesssagePage);
                    }

                    return _previousMessagePageCommand;
                }
            }

            public AutoRelayCommand NextMessagePageCommand
            {
                get
                {
                    if (_nextMessagePageCommand == null)
                    {
                        _nextMessagePageCommand = new AutoRelayCommand(() => CallApi(GetMessages, new GetMessagesArguments((VkDialog)SelectedDialog, ++MesssagePage)), () => IsApiActive && SelectedDialog != null && MesssagePage < MessagePageCount);
                        _nextMessagePageCommand.DependsOn(() => Api);
                        _nextMessagePageCommand.DependsOn(() => IsApiWorking);
                        _nextMessagePageCommand.DependsOn(() => SelectedDialog);
                        _nextMessagePageCommand.DependsOn(() => MesssagePage);
                        _nextMessagePageCommand.DependsOn(() => MessagePageCount);
                }

                    return _nextMessagePageCommand;
                }
            }

            public AutoRelayCommand SetMessagePageCommand
            {
                get
                {
                    if (_setMessagePageCommand == null)
                    {
                        _setMessagePageCommand = new AutoRelayCommand(() => CallApi(GetMessages, new GetMessagesArguments((VkDialog)SelectedDialog, MesssagePage)), () => IsApiActive && SelectedDialog != null && MesssagePage > 0 && MesssagePage < MessagePageCount);
                        _setMessagePageCommand.DependsOn(() => Api);
                        _setMessagePageCommand.DependsOn(() => IsApiWorking);
                        _setMessagePageCommand.DependsOn(() => SelectedDialog);
                        _setMessagePageCommand.DependsOn(() => MesssagePage);
                        _setMessagePageCommand.DependsOn(() => MessagePageCount);
                    }

                    return _setMessagePageCommand;
                }
            }

        public RelayCommand<string> OpenAttachmentCommand
            {
                get {
                    return _openAttachmentCommand ??
                           (_openAttachmentCommand =
                               new RelayCommand<string>(link => Process.Start(link)));
                }
            }
        #endregion

        private void InitApi()
        {
            if (Token == "")
                return;

            Api = new VkApi(Token);
            Token = "";

            CallApi(GetDialogs, (uint) 1);
        }

        private async void CallApi(Func<Task> func)
        {
            if (_api == null)
                return;

            IsApiWorking = true;
            await func();
            IsApiWorking = false;
        }

        private async void CallApi<T>(Func<T, Task> func, T param)
        {
            if (_api == null)
                return;

            IsApiWorking = true;
            await func(param);
            IsApiWorking = false;
        }

        private async Task GetDialogs(uint page)
        {
            var offset = (page - 1) * VkApi.VkMaxCount;
            var dialogs = await _api.GetDialogs(offset, VkApi.VkMaxCount);

            DialogPage = page;
            DialogPageCount = (uint) Math.Ceiling((decimal) dialogs.Count / VkApi.VkMaxCount);

            Dialogs.Clear();
            foreach (var dialog in dialogs.Dialogs)
                Dialogs.Add(dialog);
        }

        private async Task GetMessages(GetMessagesArguments parameters)
        {
            var offset = (parameters.Page - 1) * VkApi.VkMaxCount;
            var messages = await _api.GetMessages(parameters.Dialog, offset);

            MesssagePage = parameters.Page;
            MessagePageCount = (uint) Math.Ceiling((decimal) messages.Count / VkApi.VkMaxCount);

            Messages.Clear();
            foreach (var message in messages.Messages)
                Messages.Add(new VkMessageView(message));
        }
    }

    internal class GetMessagesArguments
    {
        public VkDialog Dialog { get; }
        public uint Page { get; }

        public GetMessagesArguments(VkDialog dialog, uint page)
        {
            Dialog = dialog;
            Page = page;
        }
    }
}
