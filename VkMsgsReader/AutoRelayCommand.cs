﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Command;

namespace VkMsgsReader
{
    // https://marcominerva.wordpress.com/2014/06/04/how-to-automatically-call-the-canexecute-method-when-properties-change-with-mvvm-light-in-windows-and-phone-apps/
    public class AutoRelayCommand : RelayCommand, IDisposable
    {
        private ISet<string> _properties;

        public AutoRelayCommand(Action execute)
            : base(execute)
        {
            Initialize();
        }

        public AutoRelayCommand(Action execute, Func<bool> canExecute)
            : base(execute, canExecute)
        {
            Initialize();
        }

        private void Initialize()
        {
            Messenger.Default.Register<PropertyChangedMessageBase>(this, true, (property) =>
            {
                if (_properties != null && _properties.Contains(property.PropertyName))
                    RaiseCanExecuteChanged();
            });
        }

        public void DependsOn<T>(Expression<Func<T>> propertyExpression)
        {
            if (_properties == null)
                _properties = new HashSet<string>();

            _properties.Add(GetPropertyName(propertyExpression));
        }

        private string GetPropertyName<T>(Expression<Func<T>> propertyExpression)
        {
            if (propertyExpression == null)
                throw new ArgumentNullException(nameof(propertyExpression));

            var body = propertyExpression.Body as MemberExpression;
            if (body == null)
                throw new ArgumentException("Invalid argument", nameof(propertyExpression));

            var property = body.Member as PropertyInfo;
            if (property == null)
                throw new ArgumentException("Argument is not a property", nameof(propertyExpression));

            return property.Name;
        }

        public void Dispose()
        {
            Messenger.Default.Unregister(this);
        }
    }
}
