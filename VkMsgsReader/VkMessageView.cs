﻿using System.ComponentModel;
using System.Windows.Media;
using VkMsgsReader.BL;

namespace VkMsgsReader
{
    public class VkMessageView
    {
        private readonly SolidColorBrush _outBackground = new SolidColorBrush(Colors.AntiqueWhite);
        private readonly SolidColorBrush _inBackground = new SolidColorBrush(Colors.AliceBlue);

        public VkMessageView(IVkMessage message)
        {
            Message = message;
        }

        public IVkMessage Message { get; }

        public string Name => Message.User.Name;
        public string Date => Message.Date.ToString("(HH:mm:ss dd.MM.yyyy)");
        public string Text => Message.Text;
        public SolidColorBrush InOutBackground => (Message.Outgoing) ? _outBackground : _inBackground;
        public BindingList<IVkAttachment> Attachments => new BindingList<IVkAttachment>(Message.Attachments);
    }
}
